FROM java:8

#RUN apt-get update && apt-get upgrade
ADD target/restapp.jar /opt/restapp.jar
COPY /newrelic/* /opt/newrelic/
COPY dockerrun.sh /usr/local/bin/dockerrun.sh
RUN chmod +x /usr/local/bin/dockerrun.sh
CMD ["dockerrun.sh"]
